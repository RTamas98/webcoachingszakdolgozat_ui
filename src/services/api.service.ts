import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ITrainingPlan} from "../app/components/training-plan/interfaces/training-plan.interface";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private readonly http: HttpClient) {
  }

  HOST = 'http://localhost:3000/'

  get(endpoint: string): Observable<any> {
    return this.http.get(this.HOST + endpoint);
  }

  post(endpoint: string, data: any, options?: any): Observable<any> {
    return this.http.post(this.HOST + endpoint,data,options);
  }

  put(endpoint: string, data: any): Observable<any> {
    return this.http.put(this.HOST + endpoint, data);
  }

  del(endpoint: string): Observable<any> {
    return this.http.delete(this.HOST + endpoint);
  }

  patch(endpoint: string, data: any): Observable<any> {
    return this.http.patch(this.HOST + endpoint, data);
  }

  addTrainingPlan(id: string, trainingPlan: ITrainingPlan): Observable<ITrainingPlan> {
    return this.http.post<ITrainingPlan>(this.HOST+`users/clients/trainingPlan/${id}`, trainingPlan);
  }

  updateTrainingPlan(id: string, trainingPlan: ITrainingPlan): Observable<ITrainingPlan> {
    return this.http.patch<ITrainingPlan>(this.HOST+`users/patch/clients/trainingPlan/${id}`, trainingPlan);
  }

  deleteTrainingPlan(clientId: string): Observable<ITrainingPlan> {
    return this.http.delete<ITrainingPlan>(this.HOST+`users/clients/trainingPlan/${clientId}`);
  }
}
