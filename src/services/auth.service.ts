import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private readonly api: ApiService) {
  }
  getToken() {
    return localStorage.getItem('token')
  }

  isAuth(): boolean {
    return (localStorage.getItem('token') !== undefined && localStorage.getItem('token') !== null);
  }

  logout(): void {
    localStorage.removeItem('login');
    localStorage.removeItem('token');
  }

  getCurrentUser(){
    return localStorage.getItem('username')
  }

  getUserRole() {
    return localStorage.getItem('role')
  }
}
