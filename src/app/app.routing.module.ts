import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ClientComponent } from './components/client/client.component';
import { ContentComponent } from './components/content/content.component';
import {VideosComponent} from "./components/videos/videos.component";

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent},
  { path: 'client/:id', component: ClientComponent, pathMatch: 'full'},
  { path: 'client', component: ClientComponent},
  { path: 'content', component: ContentComponent},
  { path: 'videos', component: VideosComponent},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
