import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../../../services/api.service';
import {AuthService} from "../../../services/auth.service";
import {IFile} from "./interfaces/file.interface";
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['../styles/content.component.scss']
})
export class ContentComponent implements OnInit {

  //@ts-ignore
  file: FileList;

  //@ts-ignore
  src: string;

  info: IFile[] = []

  constructor(private readonly api: ApiService, public readonly auth: AuthService, private toastService: NbToastrService) {
  }

  ngOnInit(): void {
    this.showFile()
  }

  // file event
  handleFileInput(event: any) {
    console.log(event);
    this.file = event.files;
  }

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string) {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  //update file
  async uploadFile() {
    let requestFormData: FormData = new FormData();
    console.log(this.file);
    if (this.file) {
      for (let i = 0; i < this.file.length; i++) {
        // @ts-ignore
        console.log(this.file.item(i).name)
        // @ts-ignore
        requestFormData.append('file', this.file.item(i), this.file.item(i).name);
        requestFormData.append('body', JSON.stringify({}));
        await this.api.post('files', requestFormData).toPromise();
        this.showToast('top-right','success','File successfully added.')
      }
    }
    //@ts-ignore
    this.file = undefined;
  }

  // sets the data
  async showFile() {
    const trainers = await this.api.get('users/trainers/find').toPromise()
    for (let i = 0; i < trainers.length; i++) {
      const user = await this.api.get(`users/trainers/${trainers[i].username}`).toPromise()
      for (let i = 0; i < user.files.length; i++) {
        this.info.unshift({...await this.api.get(`files/${user.files[i]}/info` ).toPromise(), link: `http://localhost:3000/files/${user.files[i]}`})
      }
    }
    return this.info;
  }

  // @ts-ignore
  videoPlayer: HTMLVideoElement;

  @ViewChild('videoPlayer')
  set mainVideoEl(el: ElementRef) {
    this.videoPlayer = el.nativeElement;
  }

  // video player
  toggleVideo(event: any) {
    this.videoPlayer.play();
  }

  //delete a file from files
  async deleteFile(id: string) {
    await this.api.del(`files/remove/${id}`).toPromise().then(()=>
      this.showToast('top-right','success','File successfully removed.')
    )
  }

}
