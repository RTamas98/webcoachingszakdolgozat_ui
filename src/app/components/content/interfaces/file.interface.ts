export interface IFile {
  _id: string;
  filename: string;
  length: number;
  chunkSize: number;
  md5: string;
  contentType: string;
  link?: string;
}
