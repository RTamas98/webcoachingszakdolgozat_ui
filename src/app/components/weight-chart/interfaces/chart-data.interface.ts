export interface IChartData {
  name: string;
  series: {
    name: string | Date;
    value: number;
  }[];
}
