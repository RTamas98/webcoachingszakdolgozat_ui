import { Component, OnInit } from '@angular/core';
import { IWeight } from './interfaces/weight.interface';
import {ApiService} from "../../../services/api.service";
import {AuthService} from "../../../services/auth.service";
import {IChartData} from "./interfaces/chart-data.interface";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-weight-chart',
  templateUrl: './weight-chart.component.html',
  styleUrls: ['../styles/weight-chart.component.scss']
})
export class WeightChartComponent implements OnInit {

  //ngx-chart
  weights: IWeight[] = [];
  id: string = '';

  constructor(private readonly api: ApiService, private readonly auth: AuthService, private router: ActivatedRoute,) { }

  async ngOnInit() {
    let user;
    if(this.auth.getUserRole()==='client'){
      user = await this.api.get(`users/client/username/${this.auth.getCurrentUser()}`).toPromise();
      this.weights = user.config.weight;
      await this.initChartData(this.weights);
    }else {
      this.router.params.subscribe(async (params) => {
        this.id = params['id'];
        console.log('asss',await this.api.get(`users/clients/${this.id}`).toPromise())
        user = await this.api.get(`users/clients/${this.id}`).toPromise()
        this.weights = user.config.weight;
        await this.initChartData(this.weights);
      });
    }
  }

  multi: IChartData[] = [];
  view: [number, number] = [700, 300];

  // options
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Date';
  yAxisLabel = 'Weight';
  timeline = true;

  colorScheme: any = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'],
  };

  async initChartData(weights: IWeight[]) {
    this.multi = [];

    const data: IChartData = {
      name: 'Weight',
      series: [],
    };

    for (const weight of weights) {
      data.series.push({
        name: new Date(weight.date),
        value: weight.weight,
      });
    }

    console.log(data);
    this.multi = [data];
  }

  onSelect(event: any) {
    console.log(event);
  }
}
