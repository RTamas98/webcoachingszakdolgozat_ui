export interface ITrainingPlan {
  dayIsIt: string;
  isSelected: boolean;
  id: any;
  name: string;
  range: string;
  set: string;
  C1R1: string;
  C1W1: string;
  C1R2: string;
  C1W2: string;
  C1R3: string;
  C1W3: string;
  C1C: string;
  C2R1: string;
  C2W1: string;
  C2R2: string;
  C2W2: string;
  C2R3: string;
  C2W3: string;
  C2C: string;
  C3R1: string;
  C3W1: string;
  C3R2: string;
  C3W2: string;
  C3R3: string;
  C3W3: string;
  C3C: string;
  C4R1: string;
  C4W1: string;
  C4R2: string;
  C4W2: string;
  C4R3: string;
  C4W3: string;
  C4C: string
  isEdit: boolean;
}



export const TrainingPlanColumns = [
  // {
  //   key: 'isSelected',
  //   type: 'isSelected',
  //   label: '',
  // },
  {
    key: 'dayIsIt',
    type: 'text',
    label: 'Day is it',
    required: true,
  },
  {
    key: 'name',
    type: 'text',
    label: 'Name of the exercise',
    required: true,
  },
  {
    key: 'set',
    type: 'text',
    label: 'Set of exercise',
    required: true,
  },
  {
    key: 'range',
    type: 'text',
    label: 'Range of the exercise',
    required: true,
  },
  {
    key: 'C1R1',
    type: 'text',
    label: 'R1',
    required: true,
  },
  {
    key: 'C1W1',
    type: 'text',
    label: 'W1',
    required: true,
  },
  {
    key: 'C1R2',
    type: 'text',
    label: 'R2',
    required: true,
  },
  {
    key: 'C1W2',
    type: 'text',
    label: 'W2',
    required: true,
  },
  {
    key: 'C1R3',
    type: 'text',
    label: 'R3',
    required: true,
  },
  {
    key: 'C1W3',
    type: 'text',
    label: 'W3',
    required: true,
  },
  {
    key: 'C1C',
    type: 'text',
    label: 'Comment',
    required: true,
  },
  {
    key: 'C2R1',
    type: 'text',
    label: 'R1',
    required: true,
  },
  {
    key: 'C2W1',
    type: 'text',
    label: 'W1',
    required: true,
  },
  {
    key: 'C2R2',
    type: 'text',
    label: 'R2',
    required: true,
  },
  {
    key: 'C2W2',
    type: 'text',
    label: 'W2',
    required: true,
  },
  {
    key: 'C2R3',
    type: 'text',
    label: 'R3',
    required: true,
  },
  {
    key: 'C2W3',
    type: 'text',
    label: 'W3',
    required: true,
  },
  {
    key: 'C2C',
    type: 'text',
    label: 'Comment',
    required: true,
  },
  {
    key: 'C3R1',
    type: 'text',
    label: 'R1',
    required: true,
  },
  {
    key: 'C3W1',
    type: 'text',
    label: 'W1',
    required: true,
  },
  {
    key: 'C3R2',
    type: 'text',
    label: 'R2',
    required: true,
  },
  {
    key: 'C3W2',
    type: 'text',
    label: 'W2',
    required: true,
  },
  {
    key: 'C3R3',
    type: 'text',
    label: 'R3',
    required: true,
  },
  {
    key: 'C3W3',
    type: 'text',
    label: 'W3',
    required: true,
  },
  {
    key: 'C3C',
    type: 'text',
    label: 'Comment',
    required: true,
  },
  {
    key: 'C4R1',
    type: 'text',
    label: 'R1',
    required: true,
  },
  {
    key: 'C4W1',
    type: 'text',
    label: 'W1',
    required: true,
  },
  {
    key: 'C4R2',
    type: 'text',
    label: 'R2',
    required: true,
  },
  {
    key: 'C4W2',
    type: 'text',
    label: 'W2',
    required: true,
  },
  {
    key: 'C4R3',
    type: 'text',
    label: 'R3',
    required: true,
  },
  {
    key: 'C4W3',
    type: 'text',
    label: 'W3',
    required: true,
  },
  {
    key: 'C4C',
    type: 'text',
    label: 'Comment',
    required: true,
  },
  {
    key: 'isEdit',
    type: 'isEdit',
    label: '',
  },
];
