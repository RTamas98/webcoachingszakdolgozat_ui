import {Component, Input, OnInit} from '@angular/core';
import {ITrainingPlan, TrainingPlanColumns} from "./interfaces/training-plan.interface";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {ApiService} from "../../../services/api.service";
import {v4 as uuidv4} from 'uuid';
import {AuthService} from "../../../services/auth.service";
import {DialogService} from "../../../services/dialog.service";
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-training-plan',
  templateUrl: './training-plan.component.html',
  styleUrls: ['../styles/training-plan.component.scss']
})
export class TrainingPlanComponent implements OnInit {

  constructor(public dialog: MatDialog, private apiService: ApiService, public readonly auth: AuthService,
              private readonly customDialogService: DialogService, private readonly toastService: NbToastrService) {
  }

  async ngOnInit() {
    // set id if the role is client
    if (this.auth.getUserRole() === 'client') {
      await this.apiService.get(`users/client/username/${this.auth.getCurrentUser()}`).subscribe(async (client) => {
          this.id = client._id;
        }
      )
    }
    await this.rendertable()
  }

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string) {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  // parent is client
  @Input()//@ts-ignore
  id: string

  displayedColumns: string[] = TrainingPlanColumns.map((col) => col.key);
  columnsSchema: any = TrainingPlanColumns;
  dataSource = new MatTableDataSource<ITrainingPlan>();
  valid: any = {};


  // set data
  async rendertable() {
    let user;
    if (this.auth.getUserRole() === 'client') {
      user = await this.apiService.get(`users/client/username/${this.auth.getCurrentUser()}`).toPromise();
    }
    if (this.auth.getUserRole() === 'trainer') {
      user = await this.apiService.get(`users/clients/${this.id}`).toPromise();
    }
    this.dataSource.data = user.config.trainingPlan;
  }

  // edits a row in a user training plan
  async editRow(row: ITrainingPlan) {
    if (row.id === 0) {
      row.id = uuidv4();
      await this.apiService.addTrainingPlan(this.id, row).subscribe((newtrainingPlan: ITrainingPlan) => {
        row.id = newtrainingPlan.id
        row.isEdit = false
        this.showToast('top-right','success','Row successfully added.')
      })
    } else {
      this.apiService.updateTrainingPlan(this.id, row).subscribe(() => (row.isEdit = false));
      this.showToast('top-right','success','Row successfully edited.')
    }

  }

  // adds an empty row into a client's training plan
  addRow() {
    const newRow: ITrainingPlan = {
      C1C: "",
      C1R1: "",
      C1R2: "",
      C1R3: "",
      C1W1: "",
      C1W2: "",
      C1W3: "",
      C2C: "",
      C2R1: "",
      C2R2: "",
      C2R3: "",
      C2W1: "",
      C2W2: "",
      C2W3: "",
      C3C: "",
      C3R1: "",
      C3R2: "",
      C3R3: "",
      C3W1: "",
      C3W2: "",
      C3W3: "",
      C4C: "",
      C4R1: "",
      C4R2: "",
      C4R3: "",
      C4W1: "",
      C4W2: "",
      C4W3: "",
      dayIsIt: "",
      id: 0,
      isEdit: false,
      isSelected: false,
      name: "",
      range: "",
      set: ""
    };
    this.dataSource.data = [...this.dataSource.data, newRow];
  }

  // removes all roves from a client's training plan
  async removeAll(id: string) {
    this.customDialogService.openConfirmDialog('Are you sure want to delete the training plan from this client ' + '<b>' + this.id  + '</b>' + '?')
      .afterClosed().subscribe(async res => {
      if (res) {
        this.showToast('top-right', 'success', 'Training plan was removed successfully.');
        await this.apiService.deleteTrainingPlan(this.id).toPromise().then(async () => await this.rendertable())
      }else {
        this.showToast('top-right', 'info', 'Training plan remove was cancelled.');
      }
    })

  }

  // handles input in a cell
  inputHandler(e: any, id: number, key: string) {
    if (!this.valid[id]) {
      this.valid[id] = {};
    }
    this.valid[id][key] = e.target.validity.valid;
  }

  disableSubmit(id: number) {
    if (this.valid[id]) {
      return Object.values(this.valid[id]).some((item) => item === false);
    }
    return false;
  }

}
