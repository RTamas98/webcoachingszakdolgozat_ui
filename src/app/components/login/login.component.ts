import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { ILogin } from './interfaces/login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../styles/login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });
  error: any;

  constructor(private api: ApiService, private router: Router) {
  }

  ngOnInit(): void {
  }

  // localstorage save setting, router link set by role
  onSubmit(formvalue: any): void {
    console.warn(this.loginForm.value);
    this.login(this.loginForm.value.username, this.loginForm.value.password).then(async (response) => {
      console.warn(response)
        localStorage.setItem('token', response.token);
        localStorage.setItem('username', response.username);
        localStorage.setItem('role', response.role);
        if(response.role !== 'client'){
          await this.router.navigate(['/registration']);
        }
        else {
          await this.router.navigate(['/client'])
        }
    }).catch(err => this.error = 'Something went wrong.')
  }

  // login
  login(username: string, password: string) {

      return this.api.post('auth/login', {
        username: username,
        password: password
      }).toPromise()

  }

}


