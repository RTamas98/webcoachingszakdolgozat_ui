import {Component, OnInit, TemplateRef} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {DialogService} from '../../../services/dialog.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NbToastrService} from "@nebular/theme";
import {FormControl, FormGroup} from "@angular/forms";
import * as moment from "moment";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['../styles/client.component.scss']
})
export class ClientComponent implements OnInit {

  role: string | null = '';
  client: any = '';
  weight: number | null = null;
  groupMember: boolean | null = null;
  expirationDay: Date = new Date();
  id = '';
  // @ts-ignore
  carbsTaken: number

  // @ts-ignore
  fatTaken: number


  // @ts-ignore
  proteinTaken: number

  // @ts-ignore
  foodInKcalTaken: number

  constructor(private readonly api: ApiService, private readonly customDialogService: DialogService,
              private router: ActivatedRoute, public readonly authService: AuthService, private modalService: NgbModal,
              private readonly toastService: NbToastrService,
  ) {
    this.role = this.authService.getUserRole();
  }

  ngOnInit(): void {
    this.renderTable();
    console.log(this.router.snapshot.params);
    this.router.params.subscribe(async (params) => {
      this.id = params['id'];
    });
  }
// create form groups
  updateNutrientsForm = new FormGroup({
    carbInGram: new FormControl(''),
    fatInGram: new FormControl(''),
    proteinInGram: new FormControl(''),
    foodInKcal: new FormControl('')
  })

  updateWeightForm = new FormGroup({
    weight: new FormControl(this.weight)
  })

  updateIntakeOfNutrientsForm = new FormGroup({
    carbInGram: new FormControl(''),
    fatInGram: new FormControl(''),
    proteinInGram: new FormControl(''),
    foodInKcal: new FormControl('')
  })


  updatePasswordForm = new FormGroup({
    password: new FormControl(''),
  })

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string): any {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  //set datas
  renderTable(): any {
    if (this.authService.getUserRole() !== 'client') {
      this.router.params.subscribe((params) => {
        this.api.get(`users/clients/${params['id']}`).subscribe((client) => {
          this.client = client;
          this.currentWeight(client);
          this.groupMember = client.config.group;
          this.expirationDay = client.config.expirationDay;
          this.calculateDaysLeft()
        });
      });
    } else {
      this.api.get(`users/client/username/${this.authService.getCurrentUser()}`).subscribe((client) => {
        this.client = client;
        this.currentWeight(client);
        this.groupMember = client.config.group;
        this.expirationDay = client.config.expirationDay;
        this.carbsTaken = client.config.intakeOfNutrients.carbInGram;
        this.fatTaken = client.config.intakeOfNutrients.fatInGram;
        this.proteinTaken = client.config.intakeOfNutrients.proteinInGram;
        this.foodInKcalTaken = client.config.intakeOfNutrients.foodInKcal
        this.calculateDaysLeft()
      })
    }
  }

  // shows the last updated weight in a user
  currentWeight(client: any) {
    let lastElement = client.config.weight.pop();
    this.weight = lastElement.weight;
  }

  // trainer can select the client's goal
  selectGoal(goal: string) {
    this.router.params.subscribe(async (params) => {
      await this.api.patch(`users/clients/${params['id']}/${goal}`, goal).toPromise();
      this.showToast('top-right', 'success', 'Goal set successfully changed.')
      this.renderTable();
    }).unsubscribe();
  }

  // trainer can select the client is training in group or not
  selectGroup(group: string) {
    this.router.params.subscribe(async (params) => {
      await this.api.patch(`users/clients/${params['id']}/set/${group}`, group).toPromise();
      this.showToast('top-right', 'success', 'Group set successfully changed.')
      this.renderTable();
    }).unsubscribe();
  }

  //modal opener
  open(modal: TemplateRef<any>) {
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title'}).result.then(async (result) => {
    });
  }


  // trainer can update the expiration day
  updateSubscriptionByDays(days: number) {
    this.api.patch(`users/clients/${this.id}/expiration`, {expirationDay: new Date(this.expirationDay).setDate(new Date(this.expirationDay).getDate() + days)}).toPromise()
      .then(this.showToast('top-right', 'success', 'Updated the expiration day successfully.'), this.renderTable())
      .catch((err) => this.showToast('top-right', 'danger', err.message));
  }

  // calculates the difference between expiration day and today date
  calculateDaysLeft() {
    const todayDate = moment(new Date());
    const expDate = moment(this.expirationDay);
    return Math.ceil(moment.duration(expDate.diff(todayDate)).asDays())
  }

  // trainer saves the nutrients for a client
  saveNutrients(carbs: number, fat: number, protein: number, kcal: number): Promise<any> {
    return this.api.patch(`users/trainers/${this.id}/nutrients`, {
      carbInGram: carbs,
      fatInGram: fat,
      proteinInGram: protein,
      foodInKcal: kcal
    }).toPromise()
  }

  // submit for nutrients
  saveNutrientsOnSubmit(formValue: any): void {
    console.warn(this.updateNutrientsForm.value);
    this.saveNutrients(this.updateNutrientsForm.value.carbInGram, this.updateNutrientsForm.value.fatInGram, this.updateNutrientsForm.value.proteinInGram, this.updateNutrientsForm.value.foodInKcal).then(() => {
      this.showToast('top-right', 'success', 'Nutrients successfully updated.')
      this.renderTable()
    }).catch(() => this.showToast('top-right', 'danger', 'Nutrients update was failed.'))
  }

  // client can set his/her weight
  saveWeight(weight: number): Promise<any> {
    return this.api.patch(`users/clients/${this.authService.getCurrentUser()}/weight`, {weight: weight}).toPromise();
  }

  // submit for weight
  saveWeightOnSubmit(formValue: any): void {
    console.warn(this.updateWeightForm.value)
    this.saveWeight(this.updateWeightForm.value.weight).then(() => {
      this.showToast('top-right', 'success', 'Weight successfully updated.');
      this.renderTable();
    }).catch(() => {
      this.showToast('top-right', 'danger', 'Weight update was failed.')
    })
  }

  // client saves the intake here
  saveIntake(carbs: number, fat: number, protein: number, kcal: number): Promise<any> {
    console.warn(`users/clients/${this.authService.getCurrentUser()}/set/intakeOfFoods`)
    return this.api.patch(`users/clients/${this.authService.getCurrentUser()}/set/intakeOfFoods`, {
      carbInGram: this.carbsTaken + carbs,
      fatInGram: this.fatTaken + fat,
      proteinInGram: this.proteinTaken + protein,
      foodInKcal: this.foodInKcalTaken + kcal
    }).toPromise()
  }

  // trainer modifies the client's password
  savePassword(password: string): Promise<any> {
    console.warn(`users/clients/${this.authService.getCurrentUser()}/set/intakeOfFoods`)
    return this.api.patch(`users/update/users/trainers/${this.id}/set/password`, {
      password: password.toString(),
    }).toPromise()
  }

  // client's password change submit
  savePasswordOnSubmit(formValue: any): void {
    console.warn(this.updatePasswordForm.value);
    this.savePassword(this.updatePasswordForm.value.password).then(() => {
      this.showToast('top-right', 'success', 'Password successfully updated.')
      this.renderTable()
    }).catch(() => this.showToast('top-right', 'danger', 'Password update was failed.'))
  }

  // client's intake change cubmit
  saveIntakeOnSubmit(formValue: any): void {
    console.warn(this.updateNutrientsForm.value);
    this.saveIntake(this.updateIntakeOfNutrientsForm.value.carbInGram, this.updateIntakeOfNutrientsForm.value.fatInGram, this.updateIntakeOfNutrientsForm.value.proteinInGram, this.updateIntakeOfNutrientsForm.value.foodInKcal).then(() => {
      this.showToast('top-right', 'success', 'Intake successfully updated.')
      this.renderTable()
    }).catch(() => this.showToast('top-right', 'danger', 'Intake update was failed.'))
  }

  // client's nutrient left for today
  calculateNutrientLeft(foodConsume: number, foodIntake: number): number {
    return foodConsume - foodIntake
  }

  // client can reset the nutrient if something went wrong
  resetIntakeOnSubmit(): Promise<any> {
    return this.api.patch(`users/clients/${this.authService.getCurrentUser()}/set/intakeOfFoods`, {
      carbInGram: 0,
      fatInGram: 0,
      proteinInGram: 0,
      foodInKcal: 0
    }).toPromise().then(() => {
      this.renderTable()
      this.showToast('top-right', 'success', 'Macros successfully set back to 0.')
    })
  }
}
