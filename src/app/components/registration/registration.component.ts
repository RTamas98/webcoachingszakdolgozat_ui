import { Component, Injectable, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { MatTableDataSource } from '@angular/material/table';
import { IPeriodicElement } from './interfaces/periodic-element.interface';
import { AuthService } from '../../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['../styles/admin-registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(private api: ApiService, private router: Router, private readonly toastService: NbToastrService,
              public authService: AuthService) {
  }

  errorTrainer: any;
  errorClient: any;
  displayedColumns: string[] = ['username', 'role', 'edit'];
  ELEMENT_DATA: IPeriodicElement[] = [];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  users: any;

  ngOnInit(): void {
    console.log(this.authService.getUserRole())
    this.renderTable()
  }

  registerTrainerForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    role: new FormControl('trainer'),
    config: new FormGroup({
      clients: new FormArray([]),
      active: new FormControl(true)
    })
  });

  foodConsumeForm = new FormGroup({
    foodInKcal: new FormControl(''),
    carbInGram: new FormControl(''),
    proteinInGram: new FormControl(''),
    fatInGram: new FormControl('')
  });

  intakeOfNutrientsForm = new FormGroup({
    foodInKcal: new FormControl(0),
    carbInGram: new FormControl(0),
    proteinInGram: new FormControl(0),
    fatInGram: new FormControl(0)
  });

  weightForm = new FormGroup({
    date: new FormControl(new Date()),
    weight: new FormControl('')
  });

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string) {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  tresholdDate = new Date();
  configForm = new FormGroup({
    expirationDay: new FormControl(new Date(new Date().getTime()+(30*24*60*60*1000))),
    group: new FormControl('false'),
    weight: this.weightForm,
    dayIsIt: new FormControl('push'),
    goal: new FormControl('bulking'),
    foodConsume: this.foodConsumeForm,
    intakeOfNutrients: this.intakeOfNutrientsForm,
    role: new FormControl('client'),
    age: new FormControl(''),
    disease: new FormControl('')
  });

  registerClientForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    role: new FormControl('client'),
    config: this.configForm
  });


  // set data
  renderTable(){
    this.api.get('users').subscribe((users: any) => {
      this.users = users
      this.ELEMENT_DATA = users;
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA)
    })
  }

  // trainer registration submit
  registerTrainerOnsubmit(formvalue: any): void {
    console.warn(this.registerTrainerForm.value);
      this.registerTrainer(this.registerTrainerForm.value.username, this.registerTrainerForm.value.password, this.registerTrainerForm.value.role, this.registerTrainerForm.value.config).then(async () => {
        this.showToast('top-right', 'success', 'Trainer successfully created.');
        await this.router.navigate(['/registration']);
        this.renderTable()
      }).catch(err => this.showToast('top-right', 'danger', 'Trainer creation was failed.'));
  }

  // trainer registration
  registerTrainer(username: string, password: string, role: 'trainer', config: { clients: [], active: true }): Promise<any> {
       return this.api.post('users/trainers', {
        username: username,
        password: password,
        role: role,
        config: config
      }).toPromise()

  }

  // client registration submit
  registerClientOnSubmit(formvalue: any): void {
    console.warn(this.registerClientForm.value);
    this.registerClient(this.registerClientForm.value.username, this.registerClientForm.value.password, this.registerClientForm.value.role, this.registerClientForm.value.config).then(async () => {
      this.showToast('top-right', 'success', 'Client successfully created.');
      await this.router.navigate(['/registration']);
      this.renderTable()
    }).catch(() =>  this.showToast('top-right', 'danger', 'Client creation was failed.'));
  }

  // client registration
  registerClient(username: string, password: string, role: 'client', config: {
    expirationDay: Date, group: boolean, weight: { date: Date, weight: number }, dayIsIt: string, goal: string, foodConsume: {
      foodInKcal: number, carbInGram: number, proteinInGram: number, fatInGram: number
    }, intakeOfNutrients: { foodInKcal: number, carbInGram: number, proteinInGram: number, fatInGram: number }, age: number, disease: string[]
  }) {
    return this.api.post('users/clients', {
      username: username,
      password: password,
      role: role,
      config: config
    }).toPromise()
  }
}
