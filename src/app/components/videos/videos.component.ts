import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {IFile} from "../content/interfaces/file.interface";
import {ApiService} from "../../../services/api.service";
import {NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['../styles/videos.component.scss']
})
export class VideosComponent implements OnInit {

//@ts-ignore
  file: FileList;

  //@ts-ignore
  src: string;

  info: IFile[] = []


  @Input()//@ts-ignore
  id;
  constructor(private readonly api: ApiService, public readonly auth: AuthService, private toastService: NbToastrService) {
  }

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string) {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  ngOnInit(): void {
    this.showFile()
  }

  // event handler
  handleFileInput(event: any) {
    console.log(event);
    this.file = event.files;
  }

  // uploads a file
  async uploadFile() {
    let requestFormData: FormData = new FormData();
    console.log(this.file);
    if (this.file) {
      for (let i = 0; i < this.file.length; i++) {
        // @ts-ignore
        console.log(this.file.item(i).name)
        // @ts-ignore
        requestFormData.append('file', this.file.item(i), this.file.item(i).name);
        requestFormData.append('body', JSON.stringify({}));
        await this.api.post('files', requestFormData).toPromise();
        this.showToast('top-right','success','File successfully added.')
      }
    }
    //@ts-ignore
    this.file = undefined;
  }

  // set data
  async showFile() {
    let user;
    if(this.auth.getUserRole() === 'client') {
      user = await this.api.get(`users/client/username/${this.auth.getCurrentUser()}`).toPromise();
    }
    if(this.auth.getUserRole() === 'trainer'){
      user = await this.api.get(`users/clients/${this.id}`).toPromise();
    }
    for (let i = 0; i < user.files.length; i++) {
      this.info.unshift({...await this.api.get(`files/${user.files[i]}/info`).toPromise(),
        link: `http://localhost:3000/files/${user.files[i]}`, _id: user.files[i]})
    }
    console.log(this.info)
    return this.info;
  }

  // @ts-ignore
  videoPlayer: HTMLVideoElement;

  @ViewChild('videoPlayer')
  set mainVideoEl(el: ElementRef) {
    this.videoPlayer = el.nativeElement;
  }

  // video player
  toggleVideo(event: any) {
    this.videoPlayer.play();
  }

  // removes all content from a client
  async removeContents() {
    await this.api.del(`files/${this.id}`).toPromise()
  }

  // removes a file from a client
  async deleteFile(id: string) {
    await this.api.del(`files/${this.id}/remove/clientsData/${id}`).toPromise().then(()=> {
      this.showToast('top-right','success','File successfully removed.')
    })
  }

}
