import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../services/api.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { DialogService } from '../../../services/dialog.service';
import { AuthService } from '../../../services/auth.service';
import { IPeriodicElement } from '../registration/interfaces/periodic-element.interface';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['../styles/modify.component.scss']
})
export class ModifyComponent implements OnInit {

  username: string = '';

  constructor(private api: ApiService, private router: Router, private readonly toastService: NbToastrService,
              private customDialogService: DialogService, public authService: AuthService, private modalService: NgbModal) {
    // @ts-ignore
    this.username = this.authService.getCurrentUser();
  }

  ngOnInit(): void {
    this.renderTable();
  }

  clientId: string = '';
  trainerId: string = '';
  trainers: any[] = [];
  clients: any[] = [];
  populatedClients: any = [];
  added: boolean = false;
  errorTrainer: any;
  errorClient: any;
  displayedColumns: string[] = ['username', 'role', 'edit'];
  displayedTrainerColumns: string[] = ['username', 'edit'];
  ELEMENT_DATA: IPeriodicElement[] = [];

  // client is the parent
  @Input()
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  dataTrainerSource = new MatTableDataSource(this.ELEMENT_DATA);
  users: any;

  // set toaster
  showToast(position: any | 'top-right' | 'bottom-right' | 'top-left' | 'bottom-left' | 'top-start',
            status: 'success' | 'info' | 'danger' | 'warning',
            message: string) {
    this.toastService.show(
      message,
      status,
      {position, status, duration: 10000, limit: 2});
  }

  // set data
  renderTable() {
    if (this.authService.getUserRole() === 'admin') {
      this.api.get('users').subscribe((users: any) => {
        this.users = users;
        this.ELEMENT_DATA = users;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      });
    } else {
      this.populatedClients = [];
      this.api.get(`users/trainers/${this.username}/clients`).subscribe((trainer: any) => {
        this.users = trainer.config.clients;
        this.ELEMENT_DATA = trainer.config.clients;
        this.dataTrainerSource = new MatTableDataSource(this.ELEMENT_DATA);
      });
    }

  }


  // add client to a trainer
  addToTrainer(trainerId: string, clientId: string) {
    this.api.patch(`users/${trainerId}/${clientId}`, {}).subscribe((res) => {
      if (res) {
        this.showToast('top-right', 'success', 'Client successfully added to trainer.');
      } else {
        this.showToast('top-right', 'danger', 'Something went wrong.');
      }
    });
  }

  // removes client from a trainer
  removeClientFromTrainer(trainerName: string, clientId: string) {
    console.log(trainerName, clientId)
    this.customDialogService.openConfirmDialog('Are you sure want to delete this client ' + '<b>' + clientId + '</b>' + ' from this trainer: ' + '<b>' + trainerName + '</b>' + '?')
      .afterClosed().subscribe(res => {
      if (res) {
        this.api.del(`users/${trainerName}/${clientId}`).subscribe(response => {
          this.showToast('top-right', 'success', 'Client was removed successfully.');
          this.renderTable();
        });
      } else {
        this.showToast('top-right', 'info', 'Client remove was cancelled.');
      }
    });
  }

  // opens trainer modal
  open(trainerModal: TemplateRef<any>, clientId: string) {
    this.clientId = clientId;
    this.api.get('users/trainers/find').subscribe((trainers: any) => {
      this.trainers = trainers;
      this.trainers = this.trainers.filter((trainer) => {
        return !trainer.config.clients.includes(this.clientId);
      });
      this.modalService.open(trainerModal, {ariaLabelledBy: 'modal-basic-title'}).result.then(async (result) => {
      });
    });
  }

  // removes a client from a trainer
  openRemove(clientModal: TemplateRef<any>, trainerId: string) {
    this.trainerId = trainerId;
    this.populatedClients = [];
    this.api.get(`users/trainers/${trainerId}/clients`).subscribe((trainer: any) => {
      this.clients = trainer.config.clients;
      for (let i = 0; i < this.clients.length; i++) {
        this.api.get(`users/clients/${this.clients[i]._id}`).subscribe((client) => {
          this.populatedClients.push(client);
        });
      }
      this.modalService.open(clientModal, {ariaLabelledBy: 'modal-basic-title'}).result.then(async (result) => {
      });
    });
  }

  // remove user from db
  removeUser(role: string, id: string) {
    this.customDialogService.openConfirmDialog('Are you sure want to delete this user: ' + '<b>' + id + '</b>' + '?')
      .afterClosed().subscribe(res => {
      if (res) {
        if (role === 'client') {
          this.api.del(`users/clients/${id}`).subscribe((response) => {
            this.showToast('top-right', 'success', 'User deleted.');
            this.renderTable();
          });
        }
        if (role === 'trainer') {
          this.api.del(`users/trainers/${id}`).subscribe((response) => {
            this.showToast('top-right', 'success', 'User deleted.');
            this.renderTable();
          });
        }
      } else {
        this.showToast('top-right', 'info', `User delete was cancelled.`);
      }
    });
  }
}
