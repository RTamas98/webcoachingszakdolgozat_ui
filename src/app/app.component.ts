import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {NbMenuItem, NbSidebarService} from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./components/styles/app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Web-Coaching';
  toggled: boolean = true;

  // @ts-ignore
  items: NbMenuItem[]

  constructor(public auth: AuthService, public sidebarService: NbSidebarService) {
  }

  // set menu
  ngOnInit(): void {
    if (this.auth.getUserRole() === 'client') {
      this.items = [
        {
          title: 'Home',
          icon: 'home-outline',
          link: '/client',
          home: false,
        },
        {
          title: 'Content',
          icon: 'video-outline',
          link: '/content',
          home: false,
        },
        {
          title: 'Asked videos',
          icon: 'video-outline',
          link: '/videos',
          home: false,
        },
      ]
    } else {
      this.items = [
        {
          title: 'Registration',
          icon: 'home-outline',
          link: '/registration',
          home: true,
        },

        {
          title: 'Content',
          icon: 'video-outline',
          link: '/content',
          home: false,
        }
      ]
    }
    // this.reCall();
  }

  reCall(){
    setTimeout(()=>{this.ngOnInit()})
  }

  // menu open/close
  toggle(): void {
    if (this.toggled) {
      this.toggled = false;
      this.sidebarService.toggle(false, 'left');
    } else {
      this.toggled = true;
      this.sidebarService.toggle(true, 'left');
    }
  }

}
